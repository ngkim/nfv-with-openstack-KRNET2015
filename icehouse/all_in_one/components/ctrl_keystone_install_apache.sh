#!/bin/bash

function ctrl_keystone_install() { 
	
    echo "
    # ------------------------------------------------------------------------------
    ### ctrl_keystone_install() !!!
    # ------------------------------------------------------------------------------"

    echo "  1 keystone 관련 패키지 설치(ntp, keystone, python-keyring)"
    apt-get -y install ntp ngrep crudini
    apt-get -y install keystone python-keyring
    apt-get -y install python-keystoneclient
    
    echo "  1-1 keystone stop"
    service keystone stop
    
    echo "  1-2 apache2 install"
    apt-get -y install python-openstackclient apache2 libapache2-mod-wsgi memcached python-memcache    
    
    echo "  2. sqlite db(/var/lib/keystone/keystone.db) 삭제"
    rm /var/lib/keystone/keystone.db
    
    echo ">>> check result ----------------------------------------------------"
    dpkg -l | egrep "keystone|python-keyring|python-keystoneclient"    
    echo "# -------------------------------------------------------------------"        
    
}
   
function ctrl_keystone_config() {
    
    echo "
    # ------------------------------------------------------------------------------
    ### ctrl_keystone_config() !!!
    # ------------------------------------------------------------------------------"    

    echo "  1 keystone 데이터베이스 생성 및 권한 설정"
 
    mysql -uroot -p$MYSQL_ROOT_PASS -e "CREATE DATABASE keystone;"
    mysql -uroot -p$MYSQL_ROOT_PASS -e "GRANT ALL ON keystone.* TO 'keystone'@'localhost' IDENTIFIED BY '$MYSQL_KEYSTONE_PASS';"
    mysql -uroot -p$MYSQL_ROOT_PASS -e "GRANT ALL ON keystone.* TO 'keystone'@'%' IDENTIFIED BY '$MYSQL_KEYSTONE_PASS';"
    
    echo "manual" > /etc/init/keystone.override

    echo "  2 keystone.conf 설정(connection, admin_token, log_dir)"
	cp ${KEYSTONE_CONF} ${KEYSTONE_CONF}_$(date +"%Y%m%d-%H%M").bak
	
	cmd="crudini --set /etc/keystone/keystone.conf DEFAULT admin_token ${SERVICE_TOKEN}"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf DEFAULT log_dir /var/log/keystone"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf database connection mysql://keystone:${MYSQL_KEYSTONE_PASS}@${MYSQL_HOST}/keystone"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf memcache servers localhost:11211"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf token provider keystone.token.providers.uuid.Provider"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf token driver keystone.token.persistence.backends.memcache.Token"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf revoke driver keystone.contrib.revoke.backends.sql.Revoke"
	run_commands $cmd
	     
    echo "  3 keystone syslog 설정"
    cmd="crudini --set /etc/keystone/keystone.conf DEFAULT use_syslog True"
	run_commands $cmd
	
	cmd="crudini --set /etc/keystone/keystone.conf DEFAULT syslog_log_facility LOG_LOCAL0"
	run_commands $cmd
    
    echo "  4 keystone db_sync(create all of the tables and configure them)"
    # LJG: db_sync가 잘 되었는지 항상 확인하는 절차가 필요함.  
    keystone-manage db_sync     
    
    
    echo "  4-1 configure the Apache HTTP server"
    
    ctrl_keystone_config_apache
    
    echo "  5 finalize installation" 
    service apache2 restart
    rm -f /var/lib/keystone/keystone.db
    
    echo ">>> check result ----------------------------------------------------"
    echo "use keystone;show table status;"            
    mysql -u root -p${MYSQL_ROOT_PASS} -h localhost -e "use keystone;show table status;"
    echo "# -------------------------------------------------------------------"

}

ctrl_keystone_config_apache() {

	WSGI_KEYSTONE_CFG="/etc/apache2/sites-available/wsgi-keystone.conf"
	
cat > ${WSGI_KEYSTONE_CFG} <<EOF
Listen 5000
Listen 35357

<VirtualHost *:5000>
    WSGIDaemonProcess keystone-public processes=5 threads=1 user=keystone display-name=%{GROUP}
    WSGIProcessGroup keystone-public
    WSGIScriptAlias / /var/www/cgi-bin/keystone/main
    WSGIApplicationGroup %{GLOBAL}
    WSGIPassAuthorization On
    <IfVersion >= 2.4>
      ErrorLogFormat "%{cu}t %M"
    </IfVersion>
    LogLevel info
    ErrorLog /var/log/apache2/keystone-error.log
    CustomLog /var/log/apache2/keystone-access.log combined
</VirtualHost>

<VirtualHost *:35357>
    WSGIDaemonProcess keystone-admin processes=5 threads=1 user=keystone display-name=%{GROUP}
    WSGIProcessGroup keystone-admin
    WSGIScriptAlias / /var/www/cgi-bin/keystone/admin
    WSGIApplicationGroup %{GLOBAL}
    WSGIPassAuthorization On
    <IfVersion >= 2.4>
      ErrorLogFormat "%{cu}t %M"
    </IfVersion>
    LogLevel info
    ErrorLog /var/log/apache2/keystone-error.log
    CustomLog /var/log/apache2/keystone-access.log combined
</VirtualHost>
EOF

	ln -s ${WSGI_KEYSTONE_CFG} /etc/apache2/sites-enabled
	mkdir -p /var/www/cgi-bin/keystone

	cmd="curl http://git.openstack.org/cgit/openstack/keystone/plain/httpd/keystone.py?h=stable/kilo | tee /var/www/cgi-bin/keystone/main /var/www/cgi-bin/keystone/admin"
	run_commands $cmd

	chown -R keystone:keystone /var/www/cgi-bin/keystone
	chmod 755 /var/www/cgi-bin/keystone/*
}

function ctrl_keystone_uninstall() {

    echo '
    # --------------------------------------------------------------------------
    ### ctrl_keystone_uninstall
    # --------------------------------------------------------------------------'    
    
    echo '  ##service keystone stop'
    service keystone stop
    
    echo '>>> before uninstall keystone ----------------------------------------'
    dpkg -l | grep keystone    
    echo '#---------------------------------------------------------------------'
    
    echo '  ##apt-get -y purge keystone'
    apt-get -y purge keystone python-keystone python-keystoneclient 
    
    echo '>>> after uninstall keystone -----------------------------------------'
    dpkg -l | grep keystone    
    echo '#---------------------------------------------------------------------'
}    

check_result_keystone() {
	# 결과 확인
	cmd="env"
	run_commands $cmd
	
    print_title_small "check result"
    cmd="keystone tenant-list"
    run_commands $cmd
    
    print_separator_small
    cmd="keystone user-list"
    run_commands $cmd
    
    print_separator_small
    cmd="keystone user-role-list"
    run_commands $cmd    
    
    print_separator    
}    

ctrl_keystone_base_user_env_create() {

    echo "
    # ------------------------------------------------------------------------------
    ### ctrl_keystone_base_user_env_create(admin tenant, admin/demo user, admin/demo rule) !!!    
    # ------------------------------------------------------------------------------"
        
    
    #
    # admin tenant 생성
        
    admin_tenant_id=$(keystone --debug tenant-list | grep "$ADMIN_TENANT " | awk '{print $2}')
    if [ $admin_tenant_id ]
    then
        printf "%s tenant already exists so delete it !!!\n" $ADMIN_TENANT
        echo "keystone tenant-delete $ADMIN_TENANT"
        keystone tenant-delete $ADMIN_TENANT
    fi
    
    cmd="keystone tenant-create --name $ADMIN_TENANT --description \"Admin Tenant\" --enabled true"
    run_commands $cmd   

    #
    # admin 사용자 생성 및 role 할당
    
    admin_user_id=$(keystone user-list | grep "$ADMIN_USER " | awk '{print $2}')
    if [ $admin_user_id ]
    then
        printf "%s user already exists so delete it !!!\n" $ADMIN_USER        
        keystone user-delete $ADMIN_USER
    fi        
    cmd="keystone user-create --name $ADMIN_USER --pass $PASSWORD --enabled true"
    run_commands $cmd
    
    #
    # admin role 생성 및 할당

    cmd="keystone role-create --name $ADMIN_ROLE"
    run_commands $cmd       
    
    cmd="keystone user-role-add --tenant $ADMIN_TENANT --user $ADMIN_USER --role $ADMIN_ROLE" 
    run_commands $cmd

    #
    # member 사용자 생성 및 role 할당
    
    member_user_id=$(keystone --os-tenant-name $ADMIN_TENANT user-list | grep "$MEMBER_USER " | awk '{print $2}')
    if [ $member_user_id ]
    then
        printf "%s user already exists so delete it !!!\n" $MEMBER_USER        
        keystone user-delete $MEMBER_USER
    fi    
    cmd="keystone user-create --name $MEMBER_USER --pass $PASSWORD --enabled true"
    run_commands $cmd
    
    #
    # member role 생성 및 할당

    cmd="keystone role-create --name $MEMBER_ROLE"
    run_commands $cmd       
    cmd="keystone user-role-add --tenant $ADMIN_TENANT --user $MEMBER_USER --role $MEMBER_ROLE"  
    run_commands $cmd

	check_result_keystone    
}


ctrl_keystone_service_create() {

    echo "
    # ------------------------------------------------------------------------------
    ### ctrl_keystone_service_create(keystone,nova,glance,cinder,neutron,ec2) !!!
    # ------------------------------------------------------------------------------"
    
    # Keystone Identity Service Endpoint
    svc_id=$(keystone service-list | grep "keystone " | awk '{print $2}')
    if [ $svc_id ]
    then
        printf "%s service already exists so delete it !!!\n" keystone        
        keystone service-delete keystone
    fi      
    cmd="keystone service-create --name keystone --type identity --description 'OpenStack Identity Service'"
    run_commands $cmd
        
    # OpenStack Compute Nova API Endpoint
    svc_id=$(keystone service-list | grep "nova " | awk '{print $2}')
    if [ $svc_id ]
    then
        printf "%s service already exists so delete it !!!\n" nova        
        keystone service-delete nova
    fi
    cmd="keystone service-create --name nova --type compute --description 'OpenStack Compute Service'"
    run_commands $cmd
    
    # Glance Image Service Endpoint
    svc_id=$(keystone service-list | grep "glance " | awk '{print $2}')
    if [ $svc_id ]
    then
        printf "%s service already exists so delete it !!!\n" glance        
        keystone service-delete glance
    fi    
    cmd="keystone service-create --name glance --type image --description 'OpenStack Image Service'"
    run_commands $cmd
    
    # Cinder Block Storage Endpoint
    svc_id=$(keystone service-list | grep "cinder " | awk '{print $2}')
    if [ $svc_id ]
    then
        printf "%s service already exists so delete it !!!\n" cinder        
        keystone service-delete cinder
    fi    
    cmd="keystone service-create --name cinder --type volume --description 'OpenStack Block Storage Service'"
    run_commands $cmd
    cmd="keystone service-create --name=cinderv2 --type=volumev2 --description='OpenStack Block Storage v2 Service'"
    run_commands $cmd
    
    # Neutron Network Service Endpoint
    svc_id=$(keystone service-list | grep "neutron " | awk '{print $2}')
    if [ $svc_id ]
    then
        printf "%s service already exists so delete it !!!\n" neutron        
        keystone service-delete neutron
    fi
    cmd="keystone service-create --name neutron --type network --description 'Neutron Network Service'"
    run_commands $cmd
    
    # OpenStack Compute EC2 API Endpoint
    svc_id=$(keystone service-list | grep "ec2 " | awk '{print $2}')
    if [ $svc_id ]
    then
        printf "%s service already exists so delete it !!!\n" ec2        
        keystone service-delete ec2
    fi
    cmd="keystone service-create --name ec2 --type ec2 --description 'EC2 Service' "
    run_commands $cmd

    echo ">>> check result -------------------------------------------------------------------"
    keystone service-list
    echo "# ------------------------------------------------------------------------------"
}

ctrl_keystone_service_endpoint_create() {

    echo "
    # ------------------------------------------------------------------------------
    ### ctrl_keystone_service_endpoint_create(keystone,nova,glance,cinder,neutron,ec2) !!!
    # ------------------------------------------------------------------------------"
    
    #Keystone OpenStack Identity Service
    KEYSTONE_SERVICE_ID=$(keystone service-list | awk '/\ keystone\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:5000/v2.0"
    ADMIN="http://$CONTROLLER_HOST:35357/v2.0"
    INTERNAL=$ADMIN
    
    echo "keystone endpoint-create
        --region $REGION
        --service_id $KEYSTONE_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"        
    
    #ask_continue_stop    
    
    keystone endpoint-create \
        --region $REGION \
        --service_id $KEYSTONE_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL
     
    #OpenStack Compute Nova API
    NOVA_SERVICE_ID=$(keystone service-list | awk '/\ nova\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:8774/v2/\$(tenant_id)s"
    ADMIN="http://$CONTROLLER_HOST:8774/v2/\$(tenant_id)s"
    INTERNAL=$ADMIN
    echo "keystone endpoint-create
        --region $REGION
        --service_id $NOVA_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"
    #ask_continue_stop

    keystone endpoint-create \
        --region $REGION \
        --service_id $NOVA_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL
     
    #OpenStack Compute EC2 API
    EC2_SERVICE_ID=$(keystone service-list | awk '/\ ec2\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:8773/services/Cloud"
    ADMIN="http://$CONTROLLER_HOST:8773/services/Admin"
    INTERNAL=$ADMIN
    echo "keystone endpoint-create
        --region $REGION
        --service_id $EC2_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"
    #ask_continue_stop

    keystone endpoint-create \
        --region $REGION \
        --service_id $EC2_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL
     
    #Glance Image Service
    GLANCE_SERVICE_ID=$(keystone service-list | awk '/\ glance\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:9292/v2"
    ADMIN="http://$CONTROLLER_HOST:9292/v2"
    INTERNAL=$ADMIN
    echo "keystone endpoint-create
        --region $REGION
        --service_id $GLANCE_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"
    #ask_continue_stop
    
    keystone endpoint-create \
        --region $REGION \
        --service_id $GLANCE_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL
        
    #
    #Cinder Block Storage Service
    
    CINDER_SERVICE_ID=$(keystone service-list | awk '/\ cinder\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:8776/v1/%(tenant_id)s"
    ADMIN="http://$CONTROLLER_HOST:8776/v1/%(tenant_id)s"
    INTERNAL=$ADMIN
    
    echo "keystone endpoint-create
        --region $REGION
        --service_id $CINDER_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"
    #ask_continue_stop
              
    keystone endpoint-create \
        --region $REGION \
        --service_id $CINDER_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL
    
    CINDER_SERVICE_ID_V2=$(keystone service-list | awk '/\ comderv2\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:8776/v2/%(tenant_id)s"
    ADMIN="http://$CONTROLLER_HOST:8776/v2/%(tenant_id)s"
    INTERNAL=$ADMIN 
    echo "keystone endpoint-create
        --region $REGION
        --service_id $CINDER_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"
    #ask_continue_stop
            
    keystone endpoint-create \
        --region $REGION \
        --service_id $CINDER_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL
     
    #Neutron Network Service
    NEUTRON_SERVICE_ID=$(keystone service-list | awk '/\ network\ / {print $2}')
    PUBLIC="http://$CONTROLLER_PUBLIC_HOST:9696"
    ADMIN="http://$CONTROLLER_HOST:9696"
    INTERNAL=$ADMIN
    echo "keystone endpoint-create
        --region $REGION
        --service_id $NEUTRON_SERVICE_ID
        --publicurl $PUBLIC
        --adminurl $ADMIN
        --internalurl $INTERNAL"
    #ask_continue_stop
        
    keystone endpoint-create \
        --region $REGION \
        --service_id $NEUTRON_SERVICE_ID \
        --publicurl $PUBLIC \
        --adminurl $ADMIN \
        --internalurl $INTERNAL

    echo ">>> check result -------------------------------------------------------------------"
    keystone endpoint-list
    echo "# ------------------------------------------------------------------------------"
}


ctrl_keystone_service_account_role_create() {

    print_title_middle "ctrl_keystone_service_account_role_create() !!!"
    
    #Service Tenant
    cmd="keystone tenant-create --name service --description \"Service Tenant\" --enabled true"
    run_commands $cmd
     
    SERVICE_TENANT_ID=$(keystone tenant-list | awk '/\ service\ / {print $2}')
    
    cmd="keystone user-create --name nova --pass nova --tenant_id $SERVICE_TENANT_ID --email nova@localhost --enabled true"
    run_commands $cmd
      
    cmd="keystone user-create --name glance --pass glance --tenant_id $SERVICE_TENANT_ID --email glance@localhost --enabled true"
    run_commands $cmd
    
    cmd="keystone user-create --name keystone --pass keystone --tenant_id $SERVICE_TENANT_ID --email keystone@localhost --enabled true"
    run_commands $cmd
    
    cmd="keystone user-create --name cinder --pass cinder --tenant_id $SERVICE_TENANT_ID --email cinder@localhost --enabled true"    
    run_commands $cmd
    
    cmd="keystone user-create --name neutron --pass neutron --tenant_id $SERVICE_TENANT_ID --email neutron@localhost --enabled true"
    run_commands $cmd
    
    ADMIN_ROLE_ID=$(keystone role-list | awk '/\ admin\ / {print $2}') 
    #Assign the nova user the admin role in service tenant
    NOVA_USER_ID=$(keystone user-list | awk '/\ nova\ / {print $2}')
    cmd="keystone user-role-add --user $NOVA_USER_ID --role $ADMIN_ROLE_ID --tenant_id $SERVICE_TENANT_ID"
    run_commands $cmd
     
    #Assign the glance user the admin role in service tenant
    GLANCE_USER_ID=$(keystone user-list | awk '/\ glance\ / {print $2}')
    cmd="keystone user-role-add --user $GLANCE_USER_ID --role $ADMIN_ROLE_ID --tenant_id $SERVICE_TENANT_ID"
    run_commands $cmd
     
    #Assign the keystone user the admin role in service tenant
    KEYSTONE_USER_ID=$(keystone user-list | awk '/\ keystone\ / {print $2}')
    cmd="keystone user-role-add --user $KEYSTONE_USER_ID --role $ADMIN_ROLE_ID --tenant_id $SERVICE_TENANT_ID"
    run_commands $cmd
    
    #Assign the cinder user the admin role in service tenant 
    CINDER_USER_ID=$(keystone user-list | awk '/\ cinder \ / {print $2}')
    cmd="keystone user-role-add --user $CINDER_USER_ID --role $ADMIN_ROLE_ID --tenant_id $SERVICE_TENANT_ID"
    run_commands $cmd
     
    #Grant admin role to neutron service user
    NEUTRON_USER_ID=$(keystone user-list | awk '/\ neutron \ / {print $2}')
    cmd="keystone user-role-add --user $NEUTRON_USER_ID --role $ADMIN_ROLE_ID --tenant_id $SERVICE_TENANT_ID"
    run_commands $cmd
    
    check_result_keystone
}
