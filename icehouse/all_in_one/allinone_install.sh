#!/bin/bash

OS_HOME=$PWD

source "$OS_HOME/utils/common_util.sh"
source "$OS_HOME/utils/control_util.sh"
source "$OS_HOME/config/default.cfg"

print_title "openstack global 환경설정"

prerequisite() {
	echo "
	    ############################################################
	        전처리
	    ############################################################
	    - 오픈스텍 계정 생성 : openstack/${PASSWORD}
	    - root 패스워드 설정
		§ ubuntu@ubuntu:~$ sudo passwd root
		§ Enter New UNIX Password :  ${PASSWORD}  
		§ Reype New Nunix password : ${PASSWORD}
	    - 우분투 Firewall 해제(이건 나중에 보안심의 이슈가 될 수 있슴)
		§ sudo ufw disable
	    - openssh 서버 설치
		§ sudo apt-get install -y openssh-server
	    - root 권한으로 외부에서 ssh 접속 허용 설정(우분투 12.04인 경우 불필요)
		  /etc/ssh/sshd_config 파일의 PermitRootLogin 설정값을 no -> yes로 변경
		  service ssh restart
	"
}

step_0() {
	print_title_middle "0) openstack install topology 설정에 따른 global_env 설정"
	
	source "./settings/00_networking_variable_setting.sh"
    print_networking_variables
    ask_continue_stop
}

step_1() {
	print_title_middle "1) allinone_global_variable_setting 설정"

	source "./settings/01_global_variable_setting.sh"
	source "./settings/02_database_variable_setting.sh"
	source "./settings/03_service_account_variable_setting.sh"
	source "./settings/04_nova_compute_variable_setting.sh"
	source "./settings/05_neutron_variable_setting.sh"
	_RET=0
}

step_2() {
	print_title_middle "2) 서버 네트워크 환경 설정"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./settings/12_allinone_network_setting.sh"
        all_in_one_hosts_info_setting
        all_in_one_NIC_setting \
    	    $CTRL_MGMT_NIC $CTRL_MGMT_IP $CTRL_MGMT_SUBNET_MASK \
    	    $CTRL_API_NIC $CTRL_API_IP $CTRL_API_SUBNET_MASK \
    	                  $CTRL_API_GW $CTRL_API_DNS \
    	    $NTWK_EXT_NIC $COM01_PRVT_NIC $COM01_WAN_NIC $COM01_LAN_NIC
	#fi
	_RET=0
}



step_3() {
	print_title_middle "3) 서버 노드 구성"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./settings/01_openstack_node_config.sh"
        server_syscontrol_change
        timezone_setting
        repository_setting
        install_base_utils
	#fi
	_RET=0
}

step_4() {
	print_title_middle "4) controller 서버에 mysql 설치"

	#ask_proceed_skip _answer	
	#if [ "$_answer" = "p" ]; then
		source "./components/ctrl_mysql_install.sh"
        ctrl_mysql_install
	#fi
	_RET=0	
}

step_5() {
	print_title_middle "5) controller 서버에 keystone 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	
    	unset OS_TENANT_NAME
    	unset OS_USERNAME
    	unset OS_PASSWORD
    	unset OS_AUTH_URL
    	unset OS_NO_CACHE

    	source "./components/ctrl_keystone_install.sh"
    	
    	cmd="export OS_SERVICE_TOKEN=${SERVICE_TOKEN}"
    	run_commands $cmd
    	
    	cmd="export OS_SERVICE_ENDPOINT=http://${KEYSTONE_ENDPOINT}:35357/v2.0/"
    	run_commands $cmd
    	
        ctrl_keystone_install
        ctrl_keystone_config
        #ask_continue_stop
        
        ctrl_keystone_base_user_env_create        
        #ask_continue_stop
        
        ctrl_keystone_service_create
        ctrl_keystone_service_endpoint_create
        ctrl_keystone_service_account_role_create

    	unset OS_SERVICE_TOKEN
    	unset OS_SERVICE_ENDPOINT
    	
cat > ~/openstack_rc <<EOF
export OS_TENANT_NAME=admin
export OS_USERNAME=admin
export OS_PASSWORD=${PASSWORD}
export OS_AUTH_URL=http://${KEYSTONE_ENDPOINT}:5000/v2.0/
export OS_NO_CACHE=1
export OS_VOLUME_API_VERSION=2
EOF

    	cat ~/openstack_rc
    	print_keystone_tenant_list

	#fi	
}

step_6() {
	print_title_middle "6) controller 서버에 glance 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
		source "./components/ctrl_glance_install.sh"
        ctrl_glance_install
        ctrl_glance_db_create
        ctrl_glance_api_registry_configure
        ctrl_glance_restart
        ctrl_glance_demo_image_create
        
        print_keystone_tenant_list
	#fi		
}

step_7() {
	print_title_middle "7) controller 서버에 cinder 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./components/ctrl_cinder_install.sh"
        ctrl_cinder_install
        ctrl_cinder_db_create
        ctrl_cinder_configure
        ctrl_cinder_restart
        
        print_keystone_tenant_list
	#fi		
}

step_8() {
	print_title_middle "8) controller 서버에 horizon 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./components/ctrl_horizon_install.sh"
        ctrl_horizon_install
        ctrl_horizon_configure
        ctrl_apache_configure_restart
	#fi	
	_RET=0
}

step_9() {
	print_title_middle "9) controller 서버에 ovs 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./components/ctrl_ovs_install.sh"
    	openvswitch_install
    	openvswitch_execute
	#fi	
	_RET=0
}

step_10() {
	print_title_middle "10) controller 서버에 neutron server, plugin 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./components/ctrl_neutron_install.sh"
        ctrl_neutron_server_and_plugin_install
        ctrl_neutron_db_create
        ctrl_neutron_server_configure
        ctrl_neutron_plugin_ml2_configure
        ctrl_neutron_l3_agent_config
        ctrl_neutron_dhcp_agent_config
        ctrl_neutron_metadata_agent_config
        ctrl_neutron_sudoers_append
        ctrl_neutron_server_restart
	#fi	
	_RET=0
}

step_11() {
	print_title_middle "11) controller 서버에 nova 설치"

	#ask_proceed_skip _answer
	#if [ "$_answer" = "p" ]; then
    	source "./components/ctrl_nova_install.sh"
        ctrl_nova_install
        ctrl_nova_db_create
        ctrl_nova_configure
        
        compute_nova_install
        compute_nova_compute_configure
        
        ctrl_nova_restart
	#fi	
	_RET=0
}

step_12() {
	print_title_middle "12) controller 서버에 rsyslog 설정 \& 재시작"

	echo "\$ModLoad imudp" >> $RSYSLOG_CONF
	echo "\$UDPServerRun 5140" >> $RSYSLOG_CONF
	echo "\$ModLoad imtcp" >> $RSYSLOG_CONF
	echo "\$InputTCPServerRun 5140" >> $RSYSLOG_CONF
	restart rsyslog
	_RET=0	
}


#print_title_middle "0) openstack install topology 설정에 따른 global_env 설정"
#print_title_middle "1) allinone_global_variable_setting 설정"
#print_title_middle "2) 서버 네트워크 환경 설정"
#print_title_middle "3) 서버 노드 구성"
#print_title_middle "4) controller 서버에 mysql 설치"
#print_title_middle "5) controller 서버에 keystone 설치"
#print_title_middle "6) controller 서버에 glance 설치"
#print_title_middle "7) controller 서버에 cinder 설치"
#print_title_middle "8) controller 서버에 horizon 설치"
#print_title_middle "9) controller 서버에 ovs 설치"
#print_title_middle "10) controller 서버에 neutron server, plugin 설치"
#print_title_middle "11) controller 서버에 nova 설치"
#print_title_middle "12) controller 서버에 rsyslog 설정 \& 재시작"

prerequisite
step_0
step_1

# 각 단계의 함수는 _RET 값을 설정: 0 이면 정상, 그렇지 않은 경우 에러로 설치 중지
_RET=0

STEP_START=2; STEP_END=6
for s in `seq $STEP_START $STEP_END`; do
	if [ $_RET -eq 0 ]; then
		eval "step_${s}"
	else
		print_red "*** Error at Step $s"
		exit	
	fi
done
