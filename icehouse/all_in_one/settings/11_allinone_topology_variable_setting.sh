#!/bin/bash

#  allinone_topology_variable_setting.sh

echo '
################################################################################

    openstack install topology 설정에 따른 variable 설정
    
        :: topology cases
            1) 2 nodes(1: controller & network, 2: compute)
            2) 3 nodes(1: controller 2: network 3: compute)
            3) 1 nodes(1: controller & network & compute)
        :: env info      
            - nodes domain name
            - nodes network env

################################################################################
'
DOMAIN_POD_HOST_CTRL=controller     # controller hostname
DOMAIN_POD_HOST_NET=nnode           # network hostname
DOMAIN_POD_HOST_BLOCK=snode         # storage(block) hostname
DOMAIN_POD_HOST_CNODE01=cnode01     # compute_01 hostname
DOMAIN_POD_HOST_CNODE02=cnode02     # compute_02 hostname

function openstack_install_allinnode_env() {

    # compute node 01
    COM01_MGMT_NIC=$CTRL_MGMT_NIC       # management network ip
    COM01_MGMT_IP=$CTRL_MGMT_IP         # management network ip
    COM01_MGMT_SUBNET_MASK=$CTRL_MGMT_SUBNET_MASK
    
    COM01_PUB_MGMT_NIC=$CTRL_API_NIC     # public management ip -> 초창기 패키지 설치시에 필요, 보안문제로 나중에는 빼야함    
    COM01_PUB_MGMT_IP=$CTRL_API_IP          # public management ip -> 초창기 패키지 설치시에 필요, 보안문제로 나중에는 빼야함
    COM01_PUB_MGMT_SUBNET_MASK=$CTRL_API_SUBNET_MASK
    COM01_PUB_MGMT_GW=$CTRL_API_GW
    COM01_PUB_MGMT_DNS=$CTRL_API_DNS
    
    COM01_PRVT_NIC=$NTWK_PRVT_NIC       # 1G NIC guest network trunk -> trunk 이므로 설정 불필요
    
    COM01_WAN_NIC=${WAN_NIC}                 # 1G NIC WAN networkt trunk -> trunk 이므로 설정 불필요
    COM01_LAN_NIC=${LAN_NIC}                 # 1G NIC LAN networkt trunk -> trunk 이므로 설정 불필요

}


