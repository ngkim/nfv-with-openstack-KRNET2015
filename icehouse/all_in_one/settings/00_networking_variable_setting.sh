#!/bin/bash

# 2015-05-29 NGKIM
# 변수의 수가 너무 많음
# 주요 변수만 설정하도록 수정할 필요가 있음

source "$OS_HOME/config/openstack_config_files"
source "$OS_HOME/utils/openstack_variables.sh"

#----------------------------------------------------------------------------------------------
# 1) controller node networking configuration
#----------------------------------------------------------------------------------------------
# 1-1) controller node: management ip address
CTRL_MGMT_NIC=${MGMT_NIC}                  # management network nic info
CTRL_MGMT_IP=$CONTROLLER_HOST           # management network ip
CTRL_MGMT_SUBNET_MASK=255.255.255.0

# 1-2) controller node: api ip address
CTRL_API_NIC=${API_NIC}                         # horizon api nic info
CTRL_API_IP=$CONTROLLER_PUBLIC_HOST     # horizon api ip
CTRL_API_SUBNET_MASK=255.255.255.0
CTRL_API_GW=${API_GW}
CTRL_API_DNS=${API_DNS}

#----------------------------------------------------------------------------------------------
# 2) network node networking configuration
#----------------------------------------------------------------------------------------------
# 2-1) network node: management ip address
NTWK_MGMT_NIC=$CTRL_MGMT_NIC        # management network nic info
NTWK_MGMT_IP=$CTRL_MGMT_IP              # management network ip
NTWK_MGMT_SUBNET_MASK=$CTRL_MGMT_SUBNET_MASK

# 2-2) network node: api ip address
NTWK_PUB_MGMT_NIC=$CTRL_API_NIC     # public management ip -> 초창기 패키지 설치시에 필요, 보안문제로 나중에는 빼야함
NTWK_PUB_MGMT_IP=$CTRL_API_IP           # public management ip -> 초창기 패키지 설치시에 필요, 보안문제로 나중에는 빼야함
NTWK_PUB_MGMT_SUBNET_MASK=$CTRL_API_SUBNET_MASK
NTWK_PUB_MGMT_GW=$CTRL_API_GW
NTWK_PUB_MGMT_DNS=$CTRL_API_DNS
    
# 2-3) network node: external network    
NTWK_EXT_NIC=${EXT_NIC}                    # external network trunk -> trunk 이므로 설정 불필요    
    
#----------------------------------------------------------------------------------------------
# 3) compute node networking configuration
#---------------------------------------------------------------
# 3-1) compute node: management ip address
COM01_MGMT_NIC=$CTRL_MGMT_NIC       # management network ip
COM01_MGMT_IP=$CTRL_MGMT_IP         # management network ip
COM01_MGMT_SUBNET_MASK=$CTRL_MGMT_SUBNET_MASK

# 3-2) compute node: api ip address
COM01_PUB_MGMT_NIC=$CTRL_API_NIC     # public management ip -> 초창기 패키지 설치시에 필요, 보안문제로 나중에는 빼야함        
COM01_PUB_MGMT_IP=$CTRL_API_IP          # public management ip -> 초창기 패키지 설치시에 필요, 보안문제로 나중에는 빼야함
COM01_PUB_MGMT_SUBNET_MASK=$CTRL_API_SUBNET_MASK
COM01_PUB_MGMT_GW=$CTRL_API_GW
COM01_PUB_MGMT_DNS=$CTRL_API_DNS

# 3-3) compute node: guest network
COM01_PRVT_NIC=$NTWK_PRVT_NIC       # 1G NIC guest network trunk -> trunk 이므로 설정 불필요

# 3-4) compute node: wan network
COM01_WAN_NIC=${WAN_NIC}                 # 1G NIC WAN networkt trunk -> trunk 이므로 설정 불필요

# 3-5) compute node: lan network
COM01_LAN_NIC=${LAN_NIC}                 # 1G NIC LAN networkt trunk -> trunk 이므로 설정 불필요
