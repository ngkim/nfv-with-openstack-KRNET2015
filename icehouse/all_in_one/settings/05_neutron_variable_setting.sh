#!/bin/bash

# 2015-05-29 NGKIM
# 변수의 수가 너무 많음
# 주요 변수만 설정하도록 수정할 필요가 있음

source "$OS_HOME/config/openstack_config_files"
source "$OS_HOME/utils/openstack_variables.sh"

# --------------------------------------------------------------------------------
#   오픈스택 neutron 설정 변수 설정 
# --------------------------------------------------------------------------------

# 0) openstack node 내부 통신
LOG_INT_BR=br-int

# 1) external network: 외부망과 network node 사이의 통신
PHY_EXT_NET=physnet_ext
LOG_EXT_BR=br-ex

# 2) guest network: compute node들과 network node 사이의 통신 
PHY_PRVT_NET=physnet_guest
PHY_PRVT_NET_RANGE=${PHY_PRVT_NET}:3001:4000
LOG_PRVT_BR=br-guest

# 3) wan network
PHY_WAN_NET=physnet_wan
PHY_WAN_NET_RANGE=${PHY_WAN_NET}:2001:3000
LOG_WAN_BR=br-wan

# 4) lan network: 
PHY_LAN_NET=physnet_lan
PHY_LAN_NET_RANGE=${PHY_LAN_NET}:10:2000
LOG_LAN_BR=br-lan

print_neutron_variables