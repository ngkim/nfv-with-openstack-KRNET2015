#!/bin/bash

# 2015-05-29 NGKIM
# MySQL variables

source "$OS_HOME/config/default.cfg"
source "$OS_HOME/config/openstack_config_files"
source "$OS_HOME/utils/openstack_variables.sh"

MYSQL_HOST=$CONTROLLER_HOST
MYSQL_ROOT_PASS=$PASSWORD

MYSQL_NOVA_PASS=nova
MYSQL_NEUTRON_PASS=neutron
MYSQL_KEYSTONE_PASS=keystone
MYSQL_GLANCE_PASS=glance
MYSQL_CINDER_PASS=cinder

print_mysql_variables
