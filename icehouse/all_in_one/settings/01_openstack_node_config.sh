#!/bin/bash

print_title "서버 커널 환경(네트워크 포워딩&필터) 수정"

function server_syscontrol_change() {
	
    print_title "### kernel setting change(ip_forward, rp_filter) to /etc/sysctl.conf!!!"
    
    echo "
    net.ipv4.ip_forward=1
    net.ipv4.conf.all.rp_filter=0
    net.ipv4.conf.default.rp_filter=0" | tee -a /etc/sysctl.conf
  
    cmd="sysctl -p"
    run_commands $cmd
  
    print_msg ">>> check result"
    print_separator    
    cat /etc/sysctl.conf    
    print_separator
}

function timezone_setting() {
    print_title "우분투 시간대를 한국시간대에 맞추기 !!!"    
    ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime
}

function repository_setting() {
    
    print_title "apt-get update & upgrade !!!"
    
    cmd="apt-get install -y python-software-properties"
    run_commands $cmd
    
    cmd="apt-get install -y ubuntu-cloud-keyring"
    run_commands $cmd
    
    #KILO_REPO_FILE="/etc/apt/sources.list.d/cloudarchive-kilo.list"
    #echo "deb http://ubuntu-cloud.archive.canonical.com/ubuntu" \
        #     "trusty-updates/kilo main" > $KILO_REPO_FILE
    #cmd="cat ${KILO_REPO_FILE}"
    #run_commands $cmd
    
    cmd="apt-get -y update"
    run_commands $cmd
    
    print_title "ubuntu desktop을 사용할 경우 아래 network-manager를 지워야 함"
    
    cmd="apt-get purge network-manager"
    run_commands $cmd
}

function install_base_utils() {
    print_title "install_base_utils !!!"
    
    cmd="apt-get -y install ntp ngrep iperf dhcpdump ipcalc"
    run_commands $cmd

}

