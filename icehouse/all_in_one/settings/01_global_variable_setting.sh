#!/bin/bash

# 2015-05-29 NGKIM
# 변수의 수가 너무 많음
# 주요 변수만 설정하도록 수정할 필요가 있음

source "$OS_HOME/config/default.cfg"
source "$OS_HOME/config/openstack_config_files"
source "$OS_HOME/utils/openstack_variables.sh"
source "$OS_HOME/utils/common_util.sh"

# 1) controller host IP 

CONTROLLER_HOST=$(set_variable_if_exist "CTRL_MGMT_IP" $CTRL_MGMT_IP_DEFAULT)
CONTROLLER_PUBLIC_HOST=$(set_variable_if_exist "CTRL_API_IP" $CTRL_API_IP_DEFAULT)    

# 2) endpoint

KEYSTONE_ENDPOINT=$CONTROLLER_HOST

SERVICE_TENANT_NAME=service
SERVICE_ENDPOINT=http://${KEYSTONE_ENDPOINT}:35357/v2.0/

# 3) admin tenant 

ADMIN_TENANT=admin

ADMIN_USER=admin
ADMIN_PASS=$PASSWORD
ADMIN_ROLE=admin

# 4) default member

MEMBER_ROLE=member

print_openstack_variables
