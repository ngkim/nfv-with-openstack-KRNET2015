#!/bin/bash

source "$OS_HOME/config/default.cfg"
source "$OS_HOME/utils/host_util.sh"

all_in_one_hosts_info_setting() {
	hosts_info_setting
	hostname_setting    
}

mgmt_NIC_setting() {
	_mgmt_nic=$1
	_mgmt_ip=$2
	_mgmt_subnet_mask=$3
	
	if [ ! -z $1 ]; then
		config_nic_static ${_mgmt_nic} ${_mgmt_ip} ${_mgmt_subnet_mask}
	fi
}

api_NIC_setting() {
	_api_nic=$1
	_api_ip=$2
	_api_subnet_mask=$3
	_api_gw=$4
	_api_dns=$5
	
	if [ ! -z $1 ]; then
		config_nic_default ${_api_nic} ${_api_ip} ${_api_subnet_mask} ${_api_gw} ${_api_dns}
	fi
}

ext_NIC_setting() {
	_ext_nic=$1
	
	if [ ! -z $1 ]; then
		config_nic_manual ${_ext_nic}
	fi	
}

guest_NIC_setting() {
	_guest_nic=$1
	
	if [ ! -z $1 ]; then
		config_nic_manual ${_guest_nic}
	fi
}

lan_NIC_setting() {
	_lan_nic=$1
	
	if [ ! -z $1 ]; then
		config_nic_manual ${_lan_nic}
	fi
}

wan_NIC_setting() {
	_wan_nic=$1
	
	if [ ! -z $1 ]; then
		config_nic_manual ${_wan_nic}
	fi
}

all_in_one_NIC_setting() {    
    
	_mgmt_nic=$1
	_mgmt_ip=$2
	_mgmt_subnet_mask=$3
	
	_api_nic=$4
	_api_ip=$5
	_api_subnet_mask=$6
	_api_gw=$7
	_api_dns=$8
	
	_ext_nic=$9
	_guest_nic=${10}
	_wan_nic=${11}
	_lan_nic=${12}
	
	config_network_interfaces
	
	mgmt_NIC_setting ${_mgmt_nic} ${_mgmt_ip} ${_mgmt_subnet_mask}
	api_NIC_setting ${_api_nic} ${_api_ip} ${_api_subnet_mask} ${_api_gw} ${_api_dns}
	ext_NIC_setting ${_ext_nic}
	guest_NIC_setting ${_guest_nic}
	lan_NIC_setting ${_wan_nic}
	wan_NIC_setting ${_lan_nic}
	
	check_network_config    
}
