#!/bin/bash

# 2015-05-29 NGKIM

source "$OS_HOME/config/openstack_config_files"
source "$OS_HOME/utils/openstack_variables.sh"

SERVICE_TENANT=service

NOVA_SERVICE_USER=nova
NOVA_SERVICE_PASS=nova

GLANCE_SERVICE_USER=glance
GLANCE_SERVICE_PASS=glance

CINDER_SERVICE_USER=cinder
CINDER_SERVICE_PASS=cinder

NEUTRON_SERVICE_USER=neutron
NEUTRON_SERVICE_PASS=neutron

print_openstack_service_account_variables
