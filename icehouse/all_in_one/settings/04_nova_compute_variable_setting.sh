#!/bin/bash

# 2015-05-29 NGKIM
# 변수의 수가 너무 많음
# 주요 변수만 설정하도록 수정할 필요가 있음

source "$OS_HOME/config/openstack_config_files"
source "$OS_HOME/utils/openstack_variables.sh"

# --------------------------------------------------------------------------------
#  NOVA-COMPUTE 설정 변수 설정
# --------------------------------------------------------------------------------"
VNC_HOST=$CONTROLLER_PUBLIC_HOST

# 외부에서 NOVNC 접속시 이용할 아이피와 포트
NOVA_NOVNC_EXT_IP=211.224.204.145
NOVA_NOVNC_EXT_PORT=26080

#NOVA_VIRT_TYPE=kvm
NOVA_VIRT_TYPE=qemu

# --------------------------------------------------------------------------------
# 오픈스택 호스트 및 이미지 변수 설정 내역
# --------------------------------------------------------------------------------"
GLANCE_HOST=$CONTROLLER_HOST

# openstack 설치시 default로 설치할 images
CIRROS_IMAGE="cirros-0.3.0-x86_64-disk.img"
UBUNTU_IMAGE="trusty-server-cloudimg-amd64-disk1.img"

print_image_variables
print_nova_compute_variables
