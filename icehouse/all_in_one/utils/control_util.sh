# ------------------------------------------------------------------------------
# interactive install을 위한 function !!!
# ------------------------------------------------------------------------------
function ask_continue_stop() {
    
    MY_PROMPT="설치 계속(c or Enter ), 정지(s)"
    while :
    do
        echo -n "$MY_PROMPT"
        read line        
        
        if [ -z "$line" ]; then
	    echo "-> continue"
	    return
	fi
        
        if [ "$line" = "c" ]; then
            echo "-> continue: $line"
            break
        elif [ "$line" = "\n" ]; then
            echo "-> newline found <$line>"
            exit
        elif [ "$line" = "s" ]; then
            echo "-> stop: $line"
            exit
        else
            echo "-> wrong input: <$line>"
            exit
        fi  
    done
} 

function ask_proceed_skip() {
    local _result_ptr=$1
    
    answer="p"
    MY_PROMPT="함수실행 진행(p), 건너뛰기(s or Enter)"
    while :
    do
        echo -n "$MY_PROMPT"
        read line        
        
        if [ -z "$line" ]; then
            answer="s"
            echo "-> skip function"            
            break
	    fi
        
        if [ "$line" = "p" ]; then
            answer="p"
            echo "-> proceed function"
            break
        elif [ "$line" = "s" ]; then
            answer="s"
            echo "-> skip function"
            break
        else
            echo "-> wrong input: <$line>"
            exit
        fi  
    done

    eval $_result_ptr=$answer
}