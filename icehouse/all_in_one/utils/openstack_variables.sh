#!/bin/bash

print_keystone_tenant_list() {
	commands="keystone tenant-list"
	
	#export OS_TENANT_NAME=admin
	#export OS_USERNAME=admin
	#export OS_PASSWORD=${PASSWORD}
	#export OS_AUTH_URL=http://${KEYSTONE_ENDPOINT}:5000/v2.0/
	#export OS_NO_CACHE=1
	#export OS_VOLUME_API_VERSION=2
	
	export OS_SERVICE_TOKEN=$SERVICE_TOKEN
	export OS_SERVICE_ENDPOINT=http://${KEYSTONE_ENDPOINT}:35357/v2.0/

    echo -e ${red}${commands}${normal}
    eval $commands
    
    _RET=$?
}

print_mysql_variables() {
	print_title_small "MySQL 변수 설정 내역"
	print_separator

	printf "%30s -> %s \n" MYSQL_HOST $MYSQL_HOST
	printf "%30s -> %s \n" MYSQL_ROOT_PASS $MYSQL_ROOT_PASS
	printf "%30s -> %s \n" MYSQL_NOVA_PASS $MYSQL_NOVA_PASS
	printf "%30s -> %s \n" MYSQL_NEUTRON_PASS $MYSQL_NEUTRON_PASS
	printf "%30s -> %s \n" MYSQL_KEYSTONE_PASS $MYSQL_KEYSTONE_PASS
	printf "%30s -> %s \n" MYSQL_GLANCE_PASS $MYSQL_GLANCE_PASS
	printf "%30s -> %s \n" MYSQL_CINDER_PASS $MYSQL_CINDER_PASS
	
	print_separator
}

print_openstack_variables() {
	print_title "오픈스택 주요계정변수 설정 내역"

	print_separator
	printf "%30s -> %s \n" CONTROLLER_HOST $CONTROLLER_HOST
	printf "%30s -> %s \n" CONTROLLER_PUBLIC_HOST $CONTROLLER_PUBLIC_HOST
	print_separator_small
	printf "%30s -> %s \n" SERVICE_TOKEN $SERVICE_TOKEN
	printf "%30s -> %s \n" SERVICE_ENDPOINT $SERVICE_ENDPOINT
	print_separator_small
	printf "%30s -> %s \n" KEYSTONE_ENDPOINT $KEYSTONE_ENDPOINT
	print_separator_small
	printf "%30s -> %s \n" SERVICE_TENANT_NAME $SERVICE_TENANT_NAME
	print_separator_small
	printf "%30s -> %s \n" MONGO_KEY $MONGO_KEY
	printf "%30s -> %s \n" PASSWORD $PASSWORD
	print_separator
}

print_openstack_service_account_variables() {
	print_title	"오픈스택 서비스 계정 설정 내역"
	
	printf "%30s -> %s \n" SERVICE_TENANT $SERVICE_TENANT
	print_separator_small
	printf "%30s -> %s \n" NOVA_SERVICE_USER $NOVA_SERVICE_USER
	printf "%30s -> %s \n" NOVA_SERVICE_PASS $NOVA_SERVICE_PASS
	print_separator_small
	printf "%30s -> %s \n" GLANCE_SERVICE_USER $GLANCE_SERVICE_USER
	printf "%30s -> %s \n" GLANCE_SERVICE_PASS $GLANCE_SERVICE_PASS
	print_separator_small
	printf "%30s -> %s \n" CINDER_SERVICE_USER $CINDER_SERVICE_USER
	printf "%30s -> %s \n" CINDER_SERVICE_PASS $CINDER_SERVICE_PASS
	print_separator_small
	printf "%30s -> %s \n" NEUTRON_SERVICE_USER $NEUTRON_SERVICE_USER
	printf "%30s -> %s \n" NEUTRON_SERVICE_PASS $NEUTRON_SERVICE_PASS
	print_separator
}

print_nova_compute_variables() {
	print_title "오픈스택 nova-compute 설정 내역"
	printf "%30s -> %s \n" NOVA_VIRT_TYPE $NOVA_VIRT_TYPE
	printf "%30s -> %s \n" NOVA_NOVNC_EXT_IP $NOVA_NOVNC_EXT_IP
	printf "%30s -> %s \n" NOVA_NOVNC_EXT_PORT $NOVA_NOVNC_EXT_PORT
	print_separator
}

print_image_variables() {
	print_title "오픈스택 호스트 및 이미지 변수 설정 내역"
	
	printf "%30s -> %s \n" VNC_HOST $VNC_HOST
	printf "%30s -> %s \n" GLANCE_HOST $GLANCE_HOST
	printf "%30s -> %s \n" CIRROS_IMAGE $CIRROS_IMAGE
	printf "%30s -> %s \n" UBUNTU_IMAGE $UBUNTU_IMAGE
	print_separator	
}

print_config_file_variables() {
	print_title "오픈스택 콤포넌트 구성정보 파일 정보 설정 내역"
		
	printf "%30s -> %s \n" KEYSTONE_CONF $KEYSTONE_CONF
	print_separator_small
	printf "%30s -> %s \n" GLANCE_API_CONF $GLANCE_API_CONF
	printf "%30s -> %s \n" GLANCE_REGISTRY_CONF $GLANCE_REGISTRY_CONF
	printf "%30s -> %s \n" GLANCE_API_INI $GLANCE_API_INI
	printf "%30s -> %s \n" GLANCE_REGISTRY_INI $GLANCE_REGISTRY_INI
	print_separator_small
	printf "%30s -> %s \n" CINDER_CONF $CINDER_CONF
	printf "%30s -> %s \n" CINDER_API $CINDER_API
	print_separator_small
	printf "%30s -> %s \n" NOVA_CONF $NOVA_CONF
	printf "%30s -> %s \n" NOVA_COMPUTE_CONF $NOVA_COMPUTE_CONF
	printf "%30s -> %s \n" NOVA_API_PASTE $NOVA_API_PASTE
	print_separator_small
	printf "%30s -> %s \n" NEUTRON_CONF $NEUTRON_CONF
	printf "%30s -> %s \n" NEUTRON_PLUGIN_ML2_CONF_INI $NEUTRON_PLUGIN_ML2_CONF_INI
	printf "%30s -> %s \n" NEUTRON_L3_AGENT_INI $NEUTRON_L3_AGENT_INI
	printf "%30s -> %s \n" NEUTRON_DHCP_AGENT_INI $NEUTRON_DHCP_AGENT_INI
	printf "%30s -> %s \n" NEUTRON_METADATA_AGENT_INI $NEUTRON_METADATA_AGENT_INI
	print_separator_small
	printf "%30s -> %s \n" HORIZON_CONF $HORIZON_CONF
	printf "%30s -> %s \n" APACHE_CONF $APACHE_CONF
	print_separator_small
	printf "%30s -> %s \n" MY_SQL_CONF $MY_SQL_CONF
	printf "%30s -> %s \n" NIC_CONF $NIC_CONF
	printf "%30s -> %s \n" RSYSLOG_CONF $RSYSLOG_CONF
	printf "%30s -> %s \n" ROOTWRAP_CONF $ROOTWRAP_CONF
	print_separator
}

print_neutron_variables() {
	print_title "오픈스택 open-vswitch 설정 내역"
	
	printf "%30s -> %s \n" PHY_EXT_NET $PHY_EXT_NET
	print_separator_small
	printf "%30s -> %s \n" PHY_PRVT_NET $PHY_PRVT_NET
	printf "%30s -> %s \n" PHY_PRVT_NET_RANGE $PHY_PRVT_NET_RANGE
	print_separator_small
	printf "%30s -> %s \n" PHY_WAN_NET $PHY_WAN_NET
	printf "%30s -> %s \n" PHY_WAN_NET_RANGE $PHY_WAN_NET_RANGE
	print_separator_small
	printf "%30s -> %s \n" PHY_LAN_NET $PHY_LAN_NET
	printf "%30s -> %s \n" PHY_LAN_NET_RANGE $PHY_LAN_NET_RANGE
	print_separator_small
	printf "%30s -> %s \n" LOG_INT_BR $LOG_INT_BR
	printf "%30s -> %s \n" LOG_EXT_BR $LOG_EXT_BR
	print_separator_small
	printf "%30s -> %s \n" LOG_PRVT_BR $LOG_PRVT_BR
	printf "%30s -> %s \n" LOG_WAN_BR $LOG_WAN_BR
	printf "%30s -> %s \n" LOG_LAN_BR $LOG_LAN_BR
	print_separator
}

print_controller_node_variables() {
	print_title_small " CTRL INFO"
	
	printf "%-30s => %-20s :: %s\n" CTRL_MGMT_NIC          $CTRL_MGMT_NIC  "controller management nic"
	printf "%-30s => %-20s :: %s\n" CTRL_MGMT_IP           $CTRL_MGMT_IP   "controller management ip"          
	printf "%-30s => %-20s :: %s\n" CTRL_MGMT_SUBNET_MASK  $CTRL_MGMT_SUBNET_MASK "controller management ip subnet mask"
	echo ""
	
	printf "%-30s => %-20s :: %s\n" CTRL_API_NIC           $CTRL_API_NIC   "controller api-server nic"
	printf "%-30s => %-20s :: %s\n" CTRL_API_IP            $CTRL_API_IP    "controller api-server ip"         
	printf "%-30s => %-20s :: %s\n" CTRL_API_SUBNET_MASK   $CTRL_API_SUBNET_MASK "controller api-server ip subnet mask"
	printf "%-30s => %-20s :: %s\n" CTRL_API_GW            $CTRL_API_GW    "controller api-server gateway ip"
	printf "%-30s => %-20s :: %s\n" CTRL_API_DNS           $CTRL_API_DNS   "controller api-server dns ip"	
	echo ""  
}

print_network_node_variables() {
	print_title_small " NTWK INFO"
    printf "%-30s => %-20s :: %s\n" NTWK_MGMT_NIC          $NTWK_MGMT_NIC  "nnode management nic"        
	printf "%-30s => %-20s :: %s\n" NTWK_MGMT_IP           $NTWK_MGMT_IP   "nnode management ip"
	printf "%-30s => %-20s :: %s\n" NTWK_MGMT_SUBNET_MASK  $NTWK_MGMT_SUBNET_MASK "nnode management ip subnet mask"
	echo ""
	
	printf "%-30s => %-20s :: %s\n" NTWK_PUB_MGMT_NIC      $NTWK_PUB_MGMT_NIC "nnode public nic(tmp, delete after install sw)"
	printf "%-30s => %-20s :: %s\n" NTWK_PUB_MGMT_IP       $NTWK_PUB_MGMT_IP   "nnode public install ip(tmp, delete after install sw)"
	printf "%-30s => %-20s :: %s\n" NTWK_PUB_MGMT_SUBNET_MASK $NTWK_PUB_MGMT_SUBNET_MASK "nnode public install ip subnet mask"
	printf "%-30s => %-20s :: %s\n" NTWK_PUB_MGMT_GW       $NTWK_PUB_MGMT_GW   "nnode public install gateway ip"
	printf "%-30s => %-20s :: %s\n" NTWK_PUB_MGMT_DNS      $NTWK_PUB_MGMT_DNS  "nnode public install dns ip"
	echo ""
	
	printf "%-30s => %-20s :: %s\n" NTWK_PRVT_NIC          $NTWK_PRVT_NIC  "nnode guest nic"
    printf "%-30s => %-20s :: %s\n" NTWK_EXT_NIC           $NTWK_EXT_NIC   "nnode external nic"
    echo ""	
}

print_compute_node_variables() {
	print_title_small " CNODE INFO"
	printf "%-30s => %-20s :: %s\n" COM01_MGMT_NIC         $COM01_MGMT_NIC     "cnode management nic"
	printf "%-30s => %-20s :: %s\n" COM01_MGMT_IP          $COM01_MGMT_IP  "cnode management ip"             
	printf "%-30s => %-20s :: %s\n" COM01_MGMT_SUBNET_MASK $COM01_MGMT_SUBNET_MASK "cnode management ip subnet mask"
	echo ""
	
	printf "%-30s => %-20s :: %s\n" COM01_PUB_MGMT_NIC     $COM01_PUB_MGMT_NIC "cnode public nic(tmp, delete after install sw)"
    printf "%-30s => %-20s :: %s\n" COM01_PUB_MGMT_IP      $COM01_PUB_MGMT_IP  "cnode public install ip(tmp, delete after install sw)"         
	printf "%-30s => %-20s :: %s\n" COM01_PUB_MGMT_SUBNET_MASK $COM01_PUB_MGMT_SUBNET_MASK "cnode public install ip subnet mask"
	printf "%-30s => %-20s :: %s\n" COM01_PUB_MGMT_GW      $COM01_PUB_MGMT_GW  "cnode public install gateway ip"       
	printf "%-30s => %-20s :: %s\n" COM01_PUB_MGMT_DNS     $COM01_PUB_MGMT_DNS "cnode public install dns ip"
	echo ""
	
    printf "%-30s => %-20s :: %s\n" COM01_PRVT_NIC         $COM01_PRVT_NIC     "cnode guest nic"
    printf "%-30s => %-20s :: %s\n" COM01_WAN_NIC          $COM01_WAN_NIC     "cnode wan nic"
    printf "%-30s => %-20s :: %s\n" COM01_LAN_NIC          $COM01_LAN_NIC     "cnode lan nic"
    echo ""  
}

print_networking_variables() {
	print_controller_node_variables
	print_network_node_variables
	print_compute_node_variables
	                
	echo '# --------------------------------------------------------------------'
	echo "  이 설정이 맞지 않으면 제대로 설치가 안되니 정확하게 확인하세요 !!!!"
    echo '# --------------------------------------------------------------------'
    echo ""	    
	
}