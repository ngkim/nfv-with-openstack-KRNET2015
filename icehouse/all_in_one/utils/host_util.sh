#!/bin/bash

function check_network_config() {
    print_title_middle "네트워크 설정 확인 !!!"
    
    cmd="route -n"
    run_commands $cmd
    print_separator
    
    cmd="ip a | grep UP"
    run_commands $cmd
    print_separator
    
    cmd="ifconfig"
    run_commands $cmd
    print_separator
}

function hosts_info_setting() {
	# 1) /etc/hosts 파일 백업
	mv /etc/hosts /etc/hosts_$(date +"%Y%m%d-%T").bak
	
	# 2) /etc/hosts 내용 구성
	cat > /etc/hosts <<EOF
${CONTROLLER_HOST} ${DOMAIN_POD_HOST_CTRL}      ${DOMAIN_POD_HOST_CTRL}.${DOMAIN_APPENDIX}
${COM01_MGMT_IP}   ${DOMAIN_POD_HOST_CNODE01}   ${DOMAIN_POD_HOST_CNODE01}.${DOMAIN_APPENDIX}
EOF

	# 3) /etc/hosts 내용 확인
	cat /etc/hosts
}

function hostname_setting() {
	# 1) 파일 백업
	mv /etc/hostname /etc/hostname_$(date +"%Y%m%d-%T").bak
    
    # 2) 설정
cat > /etc/hostname <<EOF
$DOMAIN_POD_HOST_CTRL
EOF
    
    # 3) 설정 결과 확인
    cat /etc/hostname
}

# /etc/network/interfaces 파일을 수정하여 interfaces.d 폴더의 파일을 읽도록 수정
# reboot 되어도 network환경이 적용되도록 설정
config_network_interfaces() {    
    _nic_conf="/etc/network/interfaces"
    backup_by_move $_nic_conf

cat > ${_nic_conf}<<EOF
# ------------------------------------------------------------------------------
# The loopback network interface
auto lo
iface lo inet loopback

source /etc/network/interfaces.d/*.cfg
EOF

	_nic_conf_dir="/etc/network/interfaces.d"
	mkdir -p $_nic_conf_dir	
}

config_nic_default() {
	nic_name=$1
	nic_ip=$2
	nic_subnet=$3
	nic_gw=$4
	nic_dns=$5
	
	_nic_conf_dir="/etc/network/interfaces.d"
	nic_conf="${_nic_conf_dir}/${nic_name}.cfg"
			
cat > ${nic_conf}<<EOF
# management network
auto $nic_name
iface $nic_name inet static
    address $nic_ip
    netmask $nic_subnet
    gateway $nic_gw
    # dns-* options are implemented by the resolvconf package, if installed
    dns-nameservers $nic_dns
EOF

	cmd="ifconfig $nic_name $nic_ip netmask $nic_subnet up"
	run_commands $cmd
	
	cmd="ip route add default via $nic_gw"
	run_commands $cmd
	
	# dns 설정
    echo "nameserver $_api_dns" | tee -a /etc/resolv.conf
}

config_nic_static() {
	nic_name=$1
	nic_ip=$2
	nic_subnet=$3
	
	_nic_conf_dir="/etc/network/interfaces.d"
	nic_conf="${_nic_conf_dir}/${nic_name}.cfg"
			
cat > ${nic_conf}<<EOF
# management network
auto $nic_name
iface $nic_name inet static
    address $nic_ip
    netmask $nic_subnet
EOF

	cmd="ifconfig $nic_name $nic_ip netmask $nic_subnet up"
	run_commands $cmd
}

config_nic_manual() {
	nic_name=$1
	
	_nic_conf_dir="/etc/network/interfaces.d"
	nic_conf="${_nic_conf_dir}/${nic_name}.cfg"
			
cat > ${nic_conf}<<EOF
# management network
auto $nic_name
iface $nic_name inet manual
    up ip link set dev \$IFACE up
    down ip link set dev \$IFACE down
EOF

	cmd="ifconfig $nic_name 0.0.0.0 up"
	run_commands $cmd
}