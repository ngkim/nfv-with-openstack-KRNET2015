#!/bin/bash

sudo ifconfig eth1 10.0.0.102/24 up

sudo apt-get -y update
sudo apt-get install -y git
	
sudo apt-get install -y subversion python-pip libpq-dev python-dev language-pack-en language-pack-ko
	
sudo pip install twisted
sudo pip install pyasn1
sudo pip install entropy
	
cd /vagrant/axgate
nohup python axgate-server.py 1> axgate.log 2>&1 &